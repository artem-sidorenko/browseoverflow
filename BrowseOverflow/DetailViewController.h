//
//  DetailViewController.h
//  BrowseOverflow
//
//  Created by Artem Sidorenko on 12/22/14.
//  Copyright (c) 2014 Artem Sidorenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

