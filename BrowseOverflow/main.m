//
//  main.m
//  BrowseOverflow
//
//  Created by Artem Sidorenko on 12/22/14.
//  Copyright (c) 2014 Artem Sidorenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
