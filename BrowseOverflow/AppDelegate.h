//
//  AppDelegate.h
//  BrowseOverflow
//
//  Created by Artem Sidorenko on 12/22/14.
//  Copyright (c) 2014 Artem Sidorenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

